import axios from 'axios'


let state = {
  todos: []
}

let getters = {
  getTodos: state => state.todos
}

let actions = {
  async findAllTodos({commit}) {
    let response = await axios.get('https://jsonplaceholder.typicode.com/todos')
    commit('setTodos', response.data)
  },

  async addTodo({commit}, todoTitle) {
    let todo = {
      completed: false,
      title: todoTitle
    }

    let response = await axios.post('https://jsonplaceholder.typicode.com/todos', todo)
    commit('addTodo', response.data)
  },

  async deleteTodo({commit}, id) {
    // console.log(id)
    await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    commit('deleteTodo', id)
  },

  async updateTodo({commit}, todo) {
    let response = await axios.patch(`https://jsonplaceholder.typicode.com/todos/${todo.id}`, todo.field)
    commit('updateTodo', response.data)
  }
}

let mutations = {
  setTodos(state, todos) {
    state.todos = todos
  },
  addTodo(state, todo) {
    state.todos.unshift(todo)
  },
  deleteTodo(state, id) {
    state.todos = state.todos.filter(todo => todo.id !== id)
    //state.todos.splice(id)
  },
  updateTodo(state, modifiedTodo) {
    var index = state.todos.findIndex(todo => todo.id == modifiedTodo.id)
    state.todos.splice(index, 1, modifiedTodo)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
